# Lab 3 Report
Evan Von Oehsen 

CSCI 181u

Professor Brown

2/11/2020
## First Flow
To start off, I began the lab by creating a new Node Red flow. Having never done this before, it was satisfyingly intuitive to have every availabe option ready to be dragged and dropped. Creating the every-five-second message was very simple, and below was the result:
![alternative text](img/simple-node-red.png)
The JSON returned can be found in the <strong>Appendix </strong> section.

## "Simulated" Temperature Flow
Next, we were guided through the process of creating a flow with a timed injection node followed by a function called "Thermometer," in which we could modify the value to be a random double. This was very satisfying--I started to get a sense of how we'll be able to systematically modify messages moving through the flow going forward. Below is a screenshot of the flow:
![alternative text](img/weather-flow-1.png)

As shown in the dashboard tap to the right, we also created some graphs to show the output overtime. It took some playing around to get the graphs/dashboard to look how I expected, but the result is shown below:

![alternative text](img/temp-dashboard.png)

## Open Weather Flow
For the final Node Red flow, I was guided through the process of pulling the real weather for Claremont, using the Open Weather API. Accomplishing this required installing the openweather components needed, and creating a much simpler function to pull the temperature data needed, triggered by an injection. Below is the final configuration:
![alternative text](img/weather-flow-2.png)
As you can see, the weather information was printed to the debugger logs. It also was inputted to the dashboard:
![alternative text](img/updated-dashboard.png)

By adding a separate tab to the dashboard, I was able to put the gauges side by side. (NOTE: The noticeable flatline in the original temperature graph is due to my pausing the injections as I hadn't discovered how to filter through the debugger logs yet)

## Reflection
Overall, I enjoyed this lab quite a bit. It was very satisfying how simple it was to set these processes up, and I enjoyed pulling real data at the end to create something that felt useful. I would love to play around with Node Red further on my own time to see what I can do! I also appreciated being provided instructions on how to install it on my own machine/at home.

The only setbacks I had in this project were modifying my bash file (which was resolved quickly with Prof. Brown's help) and configuring the dashboard, which took some playing around with to get right. I was surprised by how smoothly everything else went with creating the server and setting it all up.

# Appendix
 Below is my exported JSON from my first Node Red flow: 
```
[
    {
        "id": "2d2f570c.1cd358",
        "type": "tab",
        "label": "Flow 1",
        "disabled": false,
        "info": ""
    },
    {
        "id": "1199edf.0a09212",
        "type": "inject",
        "z": "2d2f570c.1cd358",
        "name": "Inject Node",
        "topic": "po181u/flow1",
        "payload": "a string!",
        "payloadType": "str",
        "repeat": "5",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 390,
        "y": 520,
        "wires": [
            [
                "95dac9f5.4708f8"
            ]
        ]
    },
    {
        "id": "95dac9f5.4708f8",
        "type": "debug",
        "z": "2d2f570c.1cd358",
        "name": "",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "true",
        "targetType": "full",
        "x": 610,
        "y": 520,
        "wires": []
    }
]
```