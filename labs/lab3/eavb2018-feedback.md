#  Feedback for Evan van Oehsen

In future lab reports it would be better to start with an overview section that described the overall report and, in each section an introduction to the section.

While I gave you the necessary javascript for your flows, in future labs you should include any javascript for nodes you create in your report.

I didn't see an explanation for the JSON

## Grading Rubric

| Component |  Task  | Points Available | Points Awarded|
|-----------|  ----- | ---------------- | --------------|
| All       | Completion           |  60 | |
|           | node-red exploration | 10 | 10 |
|           | simple flow          | 10 | 10 |
|           | dashboard + flow     | 20 | 20 |
|           | weather service      | 20 | 20 |
| Report    |                      |     | |
|           | Steps taken + issues | 20  | 20 |
|           | Screenshots          | 5  | 5 |
|           | JSON of first flow + explanation     | 5  | 3 |

