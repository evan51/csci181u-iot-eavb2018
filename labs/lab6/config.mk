

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	OPENOCD := /usr/local/Cellar/open-ocd/0.10.0/bin/openocd
	OCDSCRIPTS := /usr/local/Cellar/open-ocd/0.10.0/share/openocd/scripts
	BOARDSCRIPT  := board/st_nucleo_l476rg.cfg
        CHIBIOS  := /Users/evanvonoehsen/Desktop/181u-tools/ChibiOS-master
else
	OPENOCD := /usr/local/Cellar/open-ocd/0.10.0/bin/openocd
	OCDSCRIPTS := /usr/local/Cellar/open-ocd/0.10.0/share/openocd/scripts
	BOARDSCRIPT   := board/st_nucleo_l4.cfg
        CHIBIOS  := /Users/evanvonoehsen/Desktop/181u-tools/ChibiOS-master
endif

