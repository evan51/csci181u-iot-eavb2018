#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include <string.h>

BaseSequentialStream *chp = (BaseSequentialStream *) &SD2;
int on;
char cmd[16];
/*
 * Green LED blinker thread, times are in milliseconds.
 */
static THD_WORKING_AREA(waThread1, 256);
static THD_FUNCTION(Thread1, arg)
{

  (void)arg;
  chRegSetThreadName("blinker");
  while (true)
  {
    if (on) {
      palSetLine(LINE_LED_GREEN);         // LED on
      chThdSleepMilliseconds(500);
    } else {
      palClearLine(LINE_LED_GREEN);       // LED off
      chThdSleepMilliseconds(500);
    }
   
  }
}

void check (char *input) {
  
  if (input[0] == 'o' && input[1] == 'n') {
    on = 1;
    chprintf(chp,"blinker on\n");

  } else if (input[0] == 'o' && input[1] == 'f' && input [2] == 'f') {
    on = 0;
    chprintf(chp,"blinker off\n");

  } else {
    chprintf(chp,"bad command: -cmd-\n");

  }
}
/*
 * Application entry point.
 */
int main(void)
{
  
  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 2 using the driver default configuration.
   */
  sdStart(&SD2, NULL);

  /*
   * Creates the blinker thread.
   */

  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  
  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
  //chprintf(chp,"a format string\n",...);
  
  int index = 0;
  while (true)
  {
    uint8_t c = sdGet(&SD2);
    if (c == '\n') {
      cmd[index] = c;
      index = 0;
      check(cmd);

    } else if (index < 15) {
      cmd[index] = c;
      index++;

    } else {
      cmd[index] = '\n';
      index = 0;
      check(cmd);

    }
  }
}