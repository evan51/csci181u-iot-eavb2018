Evan Von Oehsen <br> CSCI181U <br> Prof. Brown <br> 3/10/2020

# Lab 6: Introduction to Embedded Programming
## Contents:
### "Canned" Echo Program
### Shell program
### Shell and Node-Rd
<br>

## "Canned" Echo Program
This lab was focused entirely on embedded programming. To start the lab off, we began with a simple, pre-fabricated "Echo" program which we ran on our Nucleo hardware devices.

Having never programmed embedded software, this world was unfamiliar to me; just running the echo program was a feat on its own. The goal of the program was to cause LD2, one of several small LEDs on the board, to blink, and for it to echo any text the user enters into the command line. 

I began by connecting the board to the mac using a micro-usb cord. I then had to change a directory to ensure the makefile was pointing to the right spot to access 
ChibiOS. This took more time than expected, and I kept running into the following error:

![alternative text](img/chibios-error.png)

After a glance at Professor's notes on the white board, I realized I mistook the "1" in "po181u" with a forward slash (/). Correcting this mistake solved the issue, and I was then able to navigate to the directory of the echo file, where I executed "make":

![alternative text](img/compiling.png)

After the satisfying stream of created files appeared, it was time to run it. Running the actual program entailed downloading the binary, then running goshell, an executable given to us through the lab. However, this took some trial and error, as I had to first specify the name of the board's input and in a way that was syntactically correct. Luckily, my curiosity already pointed me to the name and it was straightforward to execute, after a few tries, I was able to run the program. The light began blinking, and any value I typed into the terminal would then be spit back out on the next line.

## Shell Program
Next, it was time to make our own modified version of the program. Instead of echoing the text back, our program was to take the input and, if starting with "on" make the light shine, and of starting with "off" turn the light off. 

Compiling this program entailed the exact same steps as compiling the original "Echo" program, except running "Make Download" in the Shell directory (which was an exact copy of the echo folder). 

We then had to modify the code to take the input and modify a global boolean value (equivalent to an int in C) to decide whether the light was on or off.

Starting off was very challenging for me; the semantics for different methods used in embedded programming are much less intuitive than those in standard i/o. It took me a good amount of time to find the place where our input would be gathered, but I finally found it:
```
uint8_t c = sdGet(&SD2);
```
This "c" variable would be each character one by one that the user enters. The premise of the program was then to pace through the inputted characters, decode the message they are trying to type in, and parse out a command. To do this, I created a helper method to pace through a string that I built from the user's inputted characters:

```
void check (char *input) {
  
  if (input[0] == 'o' && input[1] == 'n') {
    on = 1;
    chprintf(chp,"blinker on\n");

  } else if (input[0] == 'o' && input[1] == 'f' && input [2] == 'f') {
    on = 0;
    chprintf(chp,"blinker off\n");

  } else {
    chprintf(chp,"bad command: -cmd-\n");

  }
}
```
This method would then check the first three characters. If the first one was 'o', we were on the right track. Anything else and the system would automatically pring out "bad command: -cmd-". The tricky part, again, was figuring out how to print that value out without the convenience of printf. Thankfully, Professor Brown generously supplied us with this. 

Writing this program took much tinkering and test prints to ensure things were working smoothly, but it ultimately made sense and was a cool thing to create knowing the little chip on the desk next to you was doing all the leg work on this one.

## Integrating with Node Red

Taking this to a whole other level, it was time to integrate with Node Red. I was excited for this step; it really was beginning to look like an IOT system we were building. I hooked up the Node Red system with the usual steps, and right away was tasked with adding "serial in" and "serial out" nodes. Three triggers inputted the simple inputs of "in", "out", and a timestamp, just to trigger that error message. The final setup looked as below:

![alternative text](img/first-node.png)

As visible from the logs in the image, the program worked as expected. 

Taking this even <strong>further</strong>, it was time to add a dashboard to this.

In order to present the values on a gague, we needed to assign numerical values to the different outputs. A function node did the trick:

![alternative text](img/node-function.png)
As you can see, it simply parsed the outputs of the Nucleo as 0 for "off", 1 for "on", and 2 for anything else (an invalid message). Even though with our buttons being so exact we would not need an error section for the gague, I was inclined to add it for practice. I also added a switch, but wanted to keep the injections as well for the timestamp specifically.

Below are screenshots of the final results, with the node configuration, and the three results of the gague:

### On:
![alternative text](img/on.png)

### Off:
![alternative text](img/off.png)

### Bad command:
![alternative text](img/bad.png)

I also added an extra text label with "status" to display the exact message.

Overall, I enjoyed this lab a lot because it was the first taste of hardware, and brought together both hardware and networking, as any good IOT system should. 
# Appendix

Blinker loop:
```
static THD_WORKING_AREA(waThread1, 256);
static THD_FUNCTION(Thread1, arg)
{

  (void)arg;
  chRegSetThreadName("blinker");
  while (true)
  {
    if (on) {
      palSetLine(LINE_LED_GREEN);         // LED on
      chThdSleepMilliseconds(500);
    }
    palClearLine(LINE_LED_GREEN);       // LED off
    chThdSleepMilliseconds(500);
  }
}
```
"Check" helper method:
```
void check (char *input) {
  
  if (input[0] == 'o' && input[1] == 'n') {
    on = 1;
    chprintf(chp,"blinker on\n");

  } else if (input[0] == 'o' && input[1] == 'f' && input [2] == 'f') {
    on = 0;
    chprintf(chp,"blinker off\n");

  } else {
    chprintf(chp,"bad command: -cmd-\n");

  }
}
```
Main method:
```
int main(void)
{
  halInit();
  chSysInit();


  sdStart(&SD2, NULL);

  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

 
  int index = 0;
  while (true)
  {
    uint8_t c = sdGet(&SD2);
    if (c == '\n') {
      cmd[index] = c;
      index = 0;
      check(cmd);

    } else if (index < 15) {
      cmd[index] = c;
      index++;

    } else {
      cmd[index] = '\n';
      index = 0;
      check(cmd);

    }
  }
```