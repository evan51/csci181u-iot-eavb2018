#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "lps22hh_reg.h"
#include "react.h"
#include "sensor.h"

#define cls()  msg_sendf("\033[2J\033[1;1H")

// LPS22HH configuration

#define LPS22HH_TIMEOUT OSAL_MS2I(50)
#define LPS22HH_ADDRESS (0xBA>>1)

static i2c_sensor_t lps22hh_handle = { &I2CD1, LPS22HH_TIMEOUT, LPS22HH_ADDRESS };

stmdev_ctx_t dev_ctx = {
  .write_reg = i2c_sensor_write,
  .read_reg = i2c_sensor_read,
  .handle = &lps22hh_handle
};


static int32_t raw_pressure;
static int16_t raw_temperature;
static float pressure_hPa;
static float temperature_degC;
static uint8_t whoamI, rst;


/*
 * Green LED blinker thread, times are in milliseconds.
 */
static THD_WORKING_AREA(waThread1, 256);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palClearPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
    palSetPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
  }
}

/*
 * Application entry point.
 */
int main(void) {
  halInit();
  chSysInit();
  i2c_sensor_init();
  msg_init();
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

  // (sanity) check device id

  whoamI = 0;
  lps22hh_device_id_get(&dev_ctx, &whoamI);
  if ( whoamI != LPS22HH_ID ) {
    msg_sendf("Who am I read failed\n");
  }

  lps22hh_reset_set(&dev_ctx, PROPERTY_ENABLE);
  do {
    lps22hh_reset_get(&dev_ctx, &rst);
  } while (rst);

  // Enable Block Data Update

  lps22hh_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);

  // Set Output Data Rate

  lps22hh_data_rate_set(&dev_ctx, LPS22HH_10_Hz_LOW_NOISE);
 
  while (true) {
    lps22hh_reg_t reg;
    
    lps22hh_read_reg(&dev_ctx, LPS22HH_STATUS, (uint8_t *)&reg, 1);

    if (reg.status.p_da) { // pressure data available ?
      lps22hh_pressure_raw_get(&dev_ctx, (uint8_t *) &raw_pressure);
      pressure_hPa = lps22hh_from_lsb_to_hpa(raw_pressure);
      msg_sendf("pressure [hPa]:%6.2f\n", pressure_hPa);
    }

    if (reg.status.t_da) { // temperature data available ?
      lps22hh_temperature_raw_get(&dev_ctx, (uint8_t *) &raw_temperature);
      temperature_degC = lps22hh_from_lsb_to_celsius(raw_temperature);
     
      msg_sendf("temperature [degC]:%6.2f\n", temperature_degC );
    }
    chThdSleepMilliseconds(500);
    cls(); // restore cursor
  }
}
