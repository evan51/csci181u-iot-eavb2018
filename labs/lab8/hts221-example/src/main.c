#include "ch.h"
#include "hal.h"
#include "hts221.h"
#include "react.h"
#include "sensor.h"

#define cls()  msg_sendf("\033[2J\033[1;1H")

/*===========================================================================*/
/* HTS221 related.                                                           */
/*===========================================================================*/

static HTS221Driver HTS221D1;

static float hygrocooked;
static float thermocooked;

/*
 * Green LED blinker thread, times are in milliseconds.
 */
static THD_WORKING_AREA(waThread1, 256);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palClearPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
    palSetPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
  }
}

int main(void) {

  halInit();
  chSysInit();
  i2c_sensor_init();
   /* HTS221 Object Initialization.*/
  hts221ObjectInit(&HTS221D1);
  hts221Start(&HTS221D1, &hts221cfg);
  
  msg_init();

  /*
   * Creates the blinker thread.
   */

  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

 
  while (true) {
    hts221HygrometerReadCooked(&HTS221D1, &hygrocooked);
    msg_sendf("Hum: %4.2f\n", hygrocooked);
    hts221ThermometerReadCooked(&HTS221D1, &thermocooked);
    msg_sendf("Temp: %4.2f\n", thermocooked);
    chThdSleepMilliseconds(200);
    cls();
  }
}
