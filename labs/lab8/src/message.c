#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "react.h"
#include <stdarg.h>
#include <string.h>

static mutex_t SDmutex_out;
static mutex_t SDmutex_in;

// Formatted send -- wrapper around chprintf code

void msg_sendf(const char *fmt, ...) {
    static char buf[256];

    // code to handle var args (...)
    
    va_list ap;
    va_start(ap,fmt);

    // lock, print formated string to buffer, transmit, unlock
    
    chMtxLock(&SDmutex_out);
    chvsnprintf(buf, sizeof(buf),fmt,ap);
    sdWrite(&SD2,(uint8_t *) buf, strlen(buf));
    chMtxUnlock(&SDmutex_out);

    // free up var args
    
    va_end(ap);
}

// send a buffer of length len

void msg_send(char *b, int len) {
    chMtxLock(&SDmutex_out);
    sdWrite(&SD2,(uint8_t *) b,len);
    chMtxUnlock(&SDmutex_out);
}

// get a '\n' terminated message with maximum length n

int msg_get(char *s, int n) {
    int i = 0;
    if (s) {
        chMtxLock(&SDmutex_in);
        for (; i < n; i++) {
            *((uint8_t *) s) = sdGet(&SD2);
            if (*s == '\n') {
                break;
            }
            s++;
        }
        *s = 0;
        chMtxUnlock(&SDmutex_in);
    }
   return i;
}

// initialize message interface.

void msg_init(void) {
     chMtxObjectInit(&SDmutex_out);
     chMtxObjectInit(&SDmutex_in);
     sdStart(&SD2, NULL);
}
