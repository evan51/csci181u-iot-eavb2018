#ifndef __REACT_H__
#define __REACT_H__

// Message interface

void msg_sendf(const char *, ...);
void msg_send(char *, int);
int  msg_get(char *, int);
void msg_init(void);

// ADC 

void adc_read(float *ts, float *vdd, float *temperature);
void adc_init(void);

#endif