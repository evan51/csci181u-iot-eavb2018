#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "hts221.h"
#include "lps22hh_reg.h"
#include "react.h"
#include "sensor.h"

#define cls()  msg_sendf("\033[2J\033[1;1H")

// LPS22HH configuration

#define LPS22HH_TIMEOUT OSAL_MS2I(50)
#define LPS22HH_ADDRESS (0xBA>>1)

static i2c_sensor_t lps22hh_handle = { &I2CD1, LPS22HH_TIMEOUT, LPS22HH_ADDRESS };

stmdev_ctx_t dev_ctx = {
  .write_reg = i2c_sensor_write,
  .read_reg = i2c_sensor_read,
  .handle = &lps22hh_handle
};


static int32_t raw_pressure;
static int16_t raw_temperature;
static float pressure_hPa;
static float temperature_degC;
static uint8_t whoamI, rst;

static HTS221Driver HTS221D1;
static float hygrocooked;
static float thermocooked;

static on = 0;

/*
 * Green LED blinker thread, times are in milliseconds.
 */
char buf[128];
static THD_WORKING_AREA(waButtonThread, 256);
static THD_FUNCTION(buttonThread, arg) {
  (void)arg;
  chRegSetThreadName("button");

  while (true) {
    int i;
    if ((i = msg_get(buf,sizeof(buf)))) {
      msg_sendf("received: %s\n\r", buf);
      //msg_sendf(buf);
      check(buf);

    } 

    chThdSleepMilliseconds(500); 
  }

  
}

static THD_WORKING_AREA(wahtsThread, 256);
static THD_FUNCTION(htsThread, arg) {
  (void)arg;
  chRegSetThreadName("hts");
  while (true) {
    hts221HygrometerReadCooked(&HTS221D1, &hygrocooked);
    //hts221ThermometerReadCooked(&HTS221D1, &thermocooked);

    chThdSleepMilliseconds(200);
    //cls();
  }
  
}

static THD_WORKING_AREA(walpsThread, 256);
static THD_FUNCTION(lpsThread, arg) {
  (void)arg;
  chRegSetThreadName("lps");

}


void check (char *input) {
  //msg_sendf(input.index('N'));
  char ledOnStr[] = "Node/write/led on";
  char ledOffStr[] = "Node/write/led off";
  char readButtonStr[] = "Node/read/button";
  char readTempStr[] = "Node/read/temperature";
  char readVoltageStr[] = "Node/read/voltage";
  char readLedStr[] = "Node/read/led";

  if (strncmp(input, ledOnStr, strlen(ledOnStr)) == 0) {
    msg_sendf("Node/status/led on\n");
    on = 1;
    palSetLine(LINE_LED_GREEN);

  } else if (strncmp(input, ledOffStr, strlen(ledOffStr)) == 0) {
    msg_sendf("Node/status/led off\n");
    on = 0;
    palClearLine(LINE_LED_GREEN);

  } else {
    msg_sendf("bad command: -cmd-\n");
  }

}

/*
 * Application entry point.
 */
int main(void) {
  /*
  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palClearPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
    palSetPad(GPIOA, GPIOA_LED_GREEN);
    chThdSleepMilliseconds(500);
  }

  */

  halInit();
  chSysInit();
  i2c_sensor_init();
  msg_init();
  
  hts221ObjectInit(&HTS221D1);
  hts221Start(&HTS221D1, &hts221cfg);

  chThdCreateStatic(waButtonThread, sizeof(waButtonThread), NORMALPRIO, buttonThread, NULL);
  chThdCreateStatic(wahtsThread, sizeof(wahtsThread), NORMALPRIO, htsThread, NULL);
  chThdCreateStatic(walpsThread, sizeof(walpsThread), NORMALPRIO, lpsThread, NULL);

  // (sanity) check device id

  whoamI = 0;
  lps22hh_device_id_get(&dev_ctx, &whoamI);
  if ( whoamI != LPS22HH_ID ) {
    msg_sendf("Who am I read failed\n");
  }

  lps22hh_reset_set(&dev_ctx, PROPERTY_ENABLE);
  do {
    lps22hh_reset_get(&dev_ctx, &rst);
  } while (rst);

  // Enable Block Data Update

  lps22hh_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);

  // Set Output Data Rate

  lps22hh_data_rate_set(&dev_ctx, LPS22HH_10_Hz_LOW_NOISE);
 
  while (true) {
    lps22hh_reg_t reg;
    
    lps22hh_read_reg(&dev_ctx, LPS22HH_STATUS, (uint8_t *)&reg, 1);

    if (reg.status.p_da) { // pressure data available ?
      lps22hh_pressure_raw_get(&dev_ctx, (uint8_t *) &raw_pressure);
      pressure_hPa = lps22hh_from_lsb_to_hpa(raw_pressure);
      msg_sendf("Node/status/lps22hh/pressure: %6.2f [hPa]\n", pressure_hPa);
    }

    if (reg.status.t_da) { // temperature data available ?
      lps22hh_temperature_raw_get(&dev_ctx, (uint8_t *) &raw_temperature);
      temperature_degC = lps22hh_from_lsb_to_celsius(raw_temperature);
      
      msg_sendf("Node/status/lps22hh/temperature:%6.2f [degC]\n", temperature_degC);
    }

    msg_sendf("Node/status/hts221/humidity: %4.2f [g/m^3]\n", hygrocooked);
    

    chThdSleepMilliseconds(500);
    cls(); // restore cursor
  }
}
