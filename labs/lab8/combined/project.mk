# List of all the board related files.
PROJECTSRC = ./src/main.c ../src/lps22hh_reg.c ../src/sensor.c ../src/message.c

# Required include directories
PROJECTINC = ./inc ../inc

# Shared variables
ALLCSRC += $(PROJECTSRC)
ALLINC  += $(PROJECTINC)
