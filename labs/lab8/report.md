
Evan Von Oehsen <br> CSCI181U <br> Prof. Brown <br> 4/03/2020

# Lab 8: Weather Station
## Contents:
### Building the Example Programs
### Combined Functionality
### Weather Node
### Control Panel
<br>

> NOTE: In this lab, I partnered with Kevin Ayala. 

## Building Example Programs
To start off, we built two separate programs pre-made for us, both of which leverage the sensors on the duaghter board which bring in new information via the i2c bus. The first one produces temperature and humidity data, pulling them on-demand via the integrated ChibiOS i2c driver every 200 milliseconds before updating the data in the output, using control characters to keep the formatting clean. The second program achieves essentially the same result as the first, yet uses the much lower-level ST driver and reads the status register to get the data it needs before updating an dcleaning, this time outputting temperature and pressure information.

Running these both involved the quick fix of changing a directory referenced in the ```crt0_v7m.o``` file (referenced in last lab's report), but once this change was made, the code compiled and downloaded successfully onto the board to be run via GoShell. The outputs for both, in order, were as below:
![](img/first-working.png)
![](img/lps-running.png)

## Combined Functionality

Now that we observed the two programs working, it was time to combine them into one cohesive program. Using a duplicate of the LPS file as a base, we started by creating a new thread for the HTS221 code to run in. We then essentially brought in the ``` hts221HygrometerReadCooked(&HTS221D1, &hygrocooked); ``` macro from the hts221-example file, put it into a while look with a proper sleep time, and called that thread a day. Every time it ran the above macro, a global variable ```hygrocooked ``` was updated. We then called the corresponding ```msg_sendf``` next to the two used for the lps code, and formatted it accordingly. Because temperature was accounted for in the lps code, we decided we did not need to include it twice. After having to deal with threads in the last lab, this process was pretty much seamless, and the resulting output was as below:

![](img/combined-working.png)

## Weather Node 

From the combined code, we could now create a functional weather node to write to an MQTT server. We started by duplicating the code from our combined functionality section. Next, we created two more threads, one for the lps sensors and one for our button, which we used code from the previous lab for. Then, we deleted the ```msg_sendf``` statements from our main function and instead pasted in the code from the previous lab to take in text inputs. We also re-used the ```check()``` function from last lab, which we created to take in a string input and decipher which message, if any, was being sent in. If recognized as a valid message, it would send the corresponding response in a format that any MQTT-enabled system could recognize. After modifying the checks and the topic strings to accomodate for pressure and humidity (as well as temperature), and testing with Goshell as usual, we were ready to go with Node-Red.

## Control Panel

The most exciting part of any lab is to get Node-Red set up. We created a flow with a SERIAL in and serial out node, function nodes to split the values from the topic strings, a switch node to determine which command was being inputted, and MQTT out and in nodes to send and receive messages from Flespi. While testing our flow, we noticed our switch was not properly able to discern which topic string was which. After using a debug node to see the input from our SERIAL node, we saw the following string:

![](img/node-red-output-error.png)

While this was likely some type of error in the source code, a quick fix was to change our switch's logic from checking whether the topic was <emph>equal</emph> to "Node/status/...", but instead to check whether the topic <emph>contains</emph> the desired value. Following this change, we noticed our Flespi MQTT dashboard immediately beginning to propagate with new messages.

Once this was complete, we began to make the dashboard. Because we had three different values (temperature, Pressure, Humidity), we created three different UI groups to keep their respective charts in their own alignments. We also added a light switch, which I copied directly from my past two labs as it has always been a satisfying method of interacting with the light. After beginning our first test, in which we tried one temperature meter, one pressure meter and graph, and one humidity graph, we noticed only one meter was working. We tried reconfiguring the other nodes several times, but could not discern why one of our meters was not displaying values, despite the others working successfully. In the end, due to copying and pasting the function nodes which pulled the values from the data inputs, we forgot to change the strings they used to isolate the value, to accomodate the fact that some strings contained temperature or pressure or humidity. After differentiating the three function nodes, our inputs worked successfully. We added the missing dashboard elements, and the result was below:
![](img/node-red-complete.png)

## Conclusion
Overall, this lab was quite manageable and enjoyable, and definitely had the least bugs/obstacles of any lab thus far. I enjoyed the amount of graphs that were used to display all the data in realtime; it almost felt like a real IoT system! This was a nice way to close out the course before the final project, as it brought all the skills we learned together (threads, Node-red, Flespi, dashboards) into one cohesive system. We also got to explore new sensors with the pressure and humidity sensors. 