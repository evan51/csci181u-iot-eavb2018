#  Feedback for Evan Van Oehsen

Your example markdown file really didn't explore many of the markdown features 

1. numbered lists
    1. with sub numbers

* Bulleted lists
   * sub lists

```python
def foo():
    pass
```

**bold** *italics* 

## Grading Rubric

| Component |  Task  | Points Available | Points Awarded|
|-----------|  ----- | ---------------- | --------------|
| All       | Completion           |  60 | 0 |
| Report    |                      |     | |
|           | Overview             | 15  | 15 |
|           | Issues               | 15  | 15 |
|Example Markdown |                |     | |
|           | Coverage of features | 8 | 4 |
|           | Image Inclusion      | 2 | 2 |

