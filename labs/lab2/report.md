# Lab 2
In this lab, I learned how to use Gitlab (SSH keys, cloning, push/pull from command line), create a markdown document (blocks, spans, images), and integrate VS code with version control (using the left sidebar to track changes, commit, and push).

## Issues that arose:

### Cloning repositories
* I had trouble cloning my repository with SSH. It was not accepting my password, which was an issue not many of my classmates were experiencing. However, creating a new SSH key solved this issue.

### Forking the class repository
* Gitlab did not want us to fork the class repository, but Professor Osborn came in after short notice and solved the issue.

### Running .code command in terminal
* Typing .code in my terminal did not open VS code as expected. A few terminal commands from Google fixed this issue promptly!

### Transitioning from double blocks to single blocks in markdown
* It took me much longer than expected to figure out how to move from a double block back to a single block in markdown. However, it ultimately just took an extra \> line! I won't be forgetting that one.

Overall, this lab was a success and I feel prepared and excited to take on some real projects.