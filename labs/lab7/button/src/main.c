#include "ch.h"
#include "hal.h"
#include "react.h"

/* Blue Button Thread */

static THD_WORKING_AREA(waThreadButton, 256);
static THD_FUNCTION(ThreadButton, arg) {
  (void)arg;
  chRegSetThreadName("button");
  
  palEnablePadEvent(GPIOC, 13U, PAL_EVENT_MODE_BOTH_EDGES);

  while (true) {
    palWaitPadTimeout(GPIOC, 13U, TIME_INFINITE);
    
    if (palReadPad(GPIOC, 13U) == PAL_HIGH) {
      if (palReadLine(LINE_LED_GREEN) == PAL_HIGH) {
        palClearLine(LINE_LED_GREEN); // LED off
        msg_sendf("Button up\n");
      }
    } else {
      if (palReadLine(LINE_LED_GREEN) == PAL_LOW) {
        palSetLine(LINE_LED_GREEN); // LED on
        msg_sendf("Button down\n");
      }
    }
  }
}

char buf[128];
int main(void) {

  /* System initialization */

  halInit();
  chSysInit();
  msg_init();

  palClearLine(LINE_LED_GREEN); // LED on

  /* Create button thread */

  chThdCreateStatic(waThreadButton, sizeof(waThreadButton), NORMALPRIO,
                    ThreadButton, NULL);

  while (true) {
    int i;
    
    if ((i = msg_get(buf,sizeof(buf))))
       msg_sendf("received: %s\n\r", buf);
    else
       chThdSleepMilliseconds(500);
    
  }
}
