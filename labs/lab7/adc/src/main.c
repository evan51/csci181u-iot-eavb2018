#include "ch.h"
#include "hal.h"
#include "react.h"

/*
 * Green LED blinker thread, times are in milliseconds.
 */

static THD_WORKING_AREA(waThread1, 256);
static THD_FUNCTION(Thread1, arg){
  (void)arg;
  chRegSetThreadName("blinker");
  while (true){
    palSetLine(LINE_LED_GREEN);         // LED on
    chThdSleepMilliseconds(500);
    palClearLine(LINE_LED_GREEN);       // LED off
    chThdSleepMilliseconds(500);
  }
}

int main(void){

  halInit();
  chSysInit();
  msg_init();
  adc_init();
  // create blinker

  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  // msg_sendf("System Clock: %d hz \r\n",STM32_HCLK);
  while (true)  {
    float ts, vdd, temperature;
    chThdSleepMilliseconds(500);

    // read adc and print values
    adc_read(&ts, &vdd, &temperature);
    msg_sendf("TS Voltage = %1.2f V\n", 3.3 * ts/4095);
    msg_sendf("Voltage     = %1.2f V\n", vdd);
    msg_sendf("Temperature = %2.1f C\n", temperature);
  }
}
