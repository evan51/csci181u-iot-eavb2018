#include "ch.h"
#include "hal.h"
#include "react.h"
#include "chprintf.h"
#include <stdio.h>
#include <string.h>

/* Blue Button Thread */
BaseSequentialStream *chp = (BaseSequentialStream *) &SD2;
int on;
static float ts, vdd, temperature;

/*
static THD_WORKING_AREA(waThreadButton, 256);
static THD_FUNCTION(ThreadButton, arg) {
  (void)arg;
  chRegSetThreadName("button");
  
  palEnablePadEvent(GPIOC, 13U, PAL_EVENT_MODE_BOTH_EDGES);

  while (true) {
    palWaitPadTimeout(GPIOC, 13U, TIME_INFINITE);
    
    if (palReadPad(GPIOC, 13U) == PAL_HIGH) {
      if (palReadLine(LINE_LED_GREEN) == PAL_HIGH) {
        palClearLine(LINE_LED_GREEN); // LED off
        msg_sendf("Button up\n");
      }
    } else {
      if (palReadLine(LINE_LED_GREEN) == PAL_LOW) {
        palSetLine(LINE_LED_GREEN); // LED on
        msg_sendf("Button down\n");
      }
    }
  }
}*/

void check (char *input) {
  //msg_sendf(input.index('N'));
  char ledOnStr[] = "Node/write/led on";
  char ledOffStr[] = "Node/write/led off";
  char readButtonStr[] = "Node/read/button";
  char readTempStr[] = "Node/read/temperature";
  char readVoltageStr[] = "Node/read/voltage";
  char readLedStr[] = "Node/read/led";

  if (strncmp(input, ledOnStr, strlen(ledOnStr)) == 0) {
    msg_sendf("Node/status/led on\n");
    on = 1;
    palSetLine(LINE_LED_GREEN);

  } else if (strncmp(input, ledOffStr, strlen(ledOffStr)) == 0) {
    msg_sendf("Node/status/led off\n");
    on = 0;
    palClearLine(LINE_LED_GREEN);

  } else if (strncmp(input, readButtonStr, strlen(readButtonStr)) == 0) {
    if (on) {
        msg_sendf( "Node/status/button down\n");

      } else {
        msg_sendf( "Node/status/button up\n");

    }

  } else if (strncmp(input, readTempStr, strlen(readTempStr)) == 0) {
    msg_sendf( "Node/status/temperature %2.1f C\n", temperature);

  } else if (strncmp(input, readVoltageStr, strlen(readVoltageStr)) == 0) {
    msg_sendf( "Node/status/voltage %1.2f V\n", vdd);

  } else if (strncmp(input, readLedStr, strlen(readLedStr)) == 0) {
    if (on) {
        msg_sendf( "Node/status/led on\n");
      } else {
        msg_sendf( "Node/status/led off\n");
      }

  } else {
    msg_sendf("bad command: -cmd-\n");
  }

  //msg_sendf(input);
  //msg_sendf("\n");
/*
  if (input[8] == 'd' ) {
    //reading a value
    if (input[10] == 'b') {
      msg_sendf("button\n");
      if (on) {
        msg_sendf("Node/status/button down\n");
      } else {
        msg_sendf("Node/status/button up\n");
      }
    } else if (input[10] == 'v') {
      //chprintf(chp,"voltage\n");
      msg_sendf("Node/status/voltage %1.2f V\n", vdd);

    } else if (input[10] == 't') {
      //chprintf(chp,"temp\n");
      msg_sendf("Node/status/temperature %2.1f C\n", temperature);

    } else if (input[10] == 'l') {
      if (on) {
        msg_sendf("Node/status/led on\n");
      } else {
        msg_sendf("Node/status/led off\n");
      }
          
    }

  } else if (input[8] == 't') {
    //writing a value
    //on = 0;

    if (input[16] == 'n') {
      msg_sendf("Node/status/led on\n");
      on = 1;
      palSetLine(LINE_LED_GREEN);

    } else if (input[16] == 'f') {
      msg_sendf("Node/status/led off\n");
      on = 0;
      palClearLine(LINE_LED_GREEN);

    }

  } else {
    msg_sendf("bad command: -cmd-\n");
    palSetLine(LINE_LED_GREEN);

  }
  */
}

/* –––––––––––––––––––––––––––––––––––––––––––––––*/


static THD_WORKING_AREA(waButtonArea, 256);

static THD_FUNCTION (buttonThread, arg) {
  (void)arg;
  chRegSetThreadName("button");
  
  palEnablePadEvent(GPIOC, 13U, PAL_EVENT_MODE_BOTH_EDGES);

  while (true) {
    palWaitPadTimeout(GPIOC, 13U, TIME_INFINITE);
    
    if (palReadPad(GPIOC, 13U) == PAL_HIGH) {
      if (palReadLine(LINE_LED_GREEN) == PAL_HIGH) {
        palClearLine(LINE_LED_GREEN); // LED off
        //msg_sendf("Node/status/button up\n");
        on = 0;
      }
    } else {
      if (palReadLine(LINE_LED_GREEN) == PAL_LOW) {
        palSetLine(LINE_LED_GREEN); // LED on
        //msg_sendf("Node/status/button down\n");
        on = 1;
      }
    }
  }
}


/* –––––––––––––––––––––––––––––––––––––––––––––––*/


static THD_WORKING_AREA(waADCArea, 256);

static THD_FUNCTION (ADCThread, arg) {
  (void)arg;
  
  // read adc and print values
  while (true)  {
    adc_read(&ts, &vdd, &temperature);
  
    chThdSleepMilliseconds(30000);
    // read adc and print values

  }
}


/* –––––––––––––––––––––––––––––––––––––––––––––––*/



static THD_WORKING_AREA(waMainArea, 256);

char buf[128];
static THD_FUNCTION (mainThread, arg) {
  (void)arg;  
  chRegSetThreadName("mainThread");

  int count = 60;
  int prev = 0;

  while (true) {
    int i;
    //msg_sendf("test");
    if ((i = msg_get(buf,sizeof(buf)))) {
      msg_sendf("received: %s\n\r", buf);
      //msg_sendf(buf);
      check(buf);

    }  
    if (!count) {
      msg_sendf("Voltage     = %1.2f V\n", vdd);
      msg_sendf("Temperature = %2.1f C\n", temperature);
      count = 60;
    } else {
      count--;
    }
    chThdSleepMilliseconds(500);
    
  }
  

}


/* –––––––––––––––––––––––––––––––––––––––––––––––*/




int main(void) {

  /* System initialization */

  halInit();
  chSysInit();
  msg_init();
  adc_init();

  palClearLine(LINE_LED_GREEN); // LED on

  /* Create button thread */
  
  
  chThdCreateStatic(waADCArea, sizeof(waADCArea), NORMALPRIO, ADCThread, NULL);

  chThdCreateStatic(waMainArea, sizeof(waMainArea), NORMALPRIO, mainThread, NULL);

  chThdCreateStatic(waButtonArea, sizeof(waButtonArea), NORMALPRIO, buttonThread, NULL);





  /*chThdCreateStatic(waThreadButton, sizeof(waThreadButton), NORMALPRIO,
                    ThreadButton, NULL);
*/

  
  int j = 10;
  while (true) {
    if (!j) {
      //msg_sendf("Node/status/Voltage = %1.2f V\n", vdd);
      //msg_sendf("Node/status/Temperaturee = %2.1f C\n", temperature);
      j = 10;

    }
    j --;
    
    chThdSleepMilliseconds(500); 
  }
  
}
