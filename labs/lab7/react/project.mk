# List of all the project related files.
PROJECTSRC = ./src/main.c ../src/message.c ../src/adc.c

# Required include directories
PROJECTINC = ./inc ../inc

# Shared variables
ALLCSRC += $(PROJECTSRC)
ALLINC  += $(PROJECTINC)
