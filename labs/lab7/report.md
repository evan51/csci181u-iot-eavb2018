
Evan Von Oehsen <br> CSCI181U <br> Prof. Brown <br> 4/03/2020

# Lab 7: Reactive Nodes
## Contents:
### Interrupt Driven Button Program
### ADC Program
### React Program
### Node-Red, Dashboard, and Flespi
### Issue transitioning to personal computer
<br>


> NOTE: In this lab, I partnered with Kevin Ayala. We picked up from the Node Red section as we both had finished up to ADC, however our screenshots and notes were both still on the lab machines. I did my best to summarize what I did in the first two sections.

## Interrupt Driven Button Program
To start off, we read up on how the buttons in our Nucleo boards work, and how filtering prevents mechanical vibrations from throwing off the shape of the signal as the button is pressed. 

We then went on to build our "button program", most of which was created for us. The basic program allowed us to type text and have it returned, and press and release the button to both see the LED shine and a text output showing it turn on and off. However, the underlying structure of the program meant that, if a user tapped quicker than the checks occurred, no light would flash. Our job to fix this was to implement interrupts.

Since using the interrupts meant only changing a few lines, I was able to finish this section quickly, with the LED reacting much more immediately to my presses and consistently. It was exciting to see interrupts in action after learning them in the lectures. 

## ADC Program

Our Analog to Digital Conversion program, or ADC, focused on taking the analog voltage signals from our stm32 boards' sensors and calculating their values to interpret and output them digitally. This involved measuring Vref–our reference voltage–and computing a V_R variable from it and using calibration data, which we could then use in our formulas to convert analog data to temperature. 

In practice, while the temperature sensor clearly was taking live readings and updating (was able to test this by placing my finger on the surprisingly small sensor and watching the values rise), they might have been slightly off–heat from the board itself is a common cause of skewed temperature values. Nonetheless, it was satisfying to see the values update and adapt to the conditions we put the thermometer in.

During my implementation of this, it took me a decent amount of time to figure out how to get the formulas set up correctly–variable names between the lab instructions and those in the lab needed to be substituted and some were more intuitive than others. My process to solve this was to paste the equation in five or six times, substituting several guesses in for each of the variables. While my best guess turned out to almost be correct, it was hard to know which variable I had wrong. The goal was to return a value somewhere around 21°C: this is about room temperature. However, while I returned a value that was higher, it changed as I interacted with the thermometer–it was also comparable to the values that other classmates were getting, so I determined it coded properly and likely involved an issue deeper in (or with the sensor itself).


## React Program 

Creating the react program involved combining our ADC and Button programs using multiple threads. There would be a ``` Button ``` thread to handle interrupt pushes to the button, an ``` ADC ``` thread to handle the updates to ADC values, and a ``` Main ``` thread, which handled all the user inputs (which we ran through a function called ``` check ```) and ``` msg_sendf ``` statements. 

Our first action plan was to configure this ``` check ``` function. I recycled this from my previous lab, and we decided to use the same method of interpreting the string commands as in the last lab, however upgrading it to take in many more commands besides just "led on" and "led off" became an arduous task. Because we focused on only significant characters within command strings (e.g. in "Node/write/led off", 'N', 'w', 'l', and 'o' would be checked for at their respective locations and if true, we did what was needed to turn the LED off). We broke these down by having if statements to check that the first topic was "Node", then that the second topic was "write" or "read", then what the rest of the message was. Any that didn't satisfy the checks would lead to a ``` msg_sendf("Bad Command") ``` message. 

While this worked well enough for our Node/read messages, it took a lot of character counting to set up. However once we finished, it was working nicely in Goshell. 

Next, we had to focus on our threads. While creating threads was pretty unfamiliar to Kevin and myself, we dug into the lecture slides, Chibios's online docs on threads, and ultimately found we could use the structure of the button thread as a template for the other two threads to create. We then made our two other threads, and essentially pasted in the ADC code where it was needed and code in the main thread; both used while loops with checks to ``` adc_read ``` and text inputs respectively. 

As we started to test, our main thread worked properly, without a hitch. However, our ADC thread was seemingly not reading the sensors' values and updating our static variables as our main thread was printing 0.0 for each value. We began to troubleshoot as to why, moving around our ``` adc_init() ``` initializer among other components. After talking to Professor Brown, we realized that we were handling all of it in our ADC thread when we were supposed to handle it in the main method. Making these changes–and clearing away some of the junk we copied from the original ADC program that we didn't need–we were able to get the thread to work successfully. Below are all the working responses:

![](img/working-goshell-responses.png)

> note: the "??????down" message was due to the button being angled when pressed and seemed to be a common ussue amongst classmates. It worked as expected when pushed straight downward. Also, the "up" and "down" after Node/status were mistakenly added at the ends and can be disregarded.

## Node-Red, Dashboard, and Flespi

When it came time to start with Node Red, we figured we were done with coding. As we began to set up our flows, however, our board was not registerring the commands we attempted to inject into it. The LED would not change, and we were receiving nothing but fragmented messages in response to our requests to get temperature and voltage. After a few frustrating hours, we noticed that our outputs were being split between goshell and node-red:

![](img/split-outputs.png)

Of course, after re-evaluating why this was happening, we remembered that once running ```make download```, the device automaticallly runs the code as necessary and that goshell was just a way to communicate to it via terminal. After quitting the goshell, the issue quickly resolved. At least, for certain commands.

While all of our ```Node/read``` messages were being recognized by the board now, our LED changing messages were still not working. After talking to Professor Brown, we realized what the issue was right away: the character counting method of parsing messages, as outlined above, was just not going to cut it. We had tried various other methods, including a Switch/Case structure as shown below, but were unable to get it working:

![](img/doesnt_work.png)

Finally, we decided to use strcmp to compare strings. This made our code much more fluid and intuitive to understand, and the Nucleo board seemed to appreciate the change as it immediately became compatible with our node red.

After we got our baseline working, we made a few modifications, integrating with Flespi (which happened without a hitch surprisingly enough), and adding a dashboard with gagues for temperature and voltage and a switch to turn the LED on and off. 

Our final node red flow:

![](img/node-red-layout.png) 

...dashboard with LED on:

![](img/dashboard-led-on.png) 

...and dashboard with LED off:

![](img/dashboard-led-off.png) 


## Issue transitioning to personal computer

Following completion of the ADC program, we were forced to move back home due to the recent COVID pandemic. Due to this, we needed to go through the process of setting everything up on our personal machines. While most of this was straightforward, a blocker arose:

```
    *** No rule to make target `/common/cs/cs181u/ChibiOS/os/common/startup/ARMCMx/compilers/GCC/crt0_v7m.S', needed by `build/obj/crt0_v7m.o'.  Stop.

```
This error message was being returned during my initial few runs of ```make download```. While I already knew the config.mk file was correct, I decided to try to get my lab6 working first to debug the issue. To solve it, I had to dig in to find crt0_v7m.0 in the build/obj folder. Once doing this, I changed the directory to be that of the ChibiOS file on my own machine. The file I made the changes in looked as below:

![](img/file-bug-after-fix.png)

Most of the file was unreadable, but the parts I needed to change were clear with that common/cs/cs181u/... directory (in the photo is already changed to my directory). Once this change was made in each project's respective crt0_v7m.o file, and I allowed all the processes from 'unverified developers' to run in my privacy settings, things ran as expected.
