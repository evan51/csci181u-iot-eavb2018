#include "ch.h"
#include "hal.h"
#include "react.h"

// location of calibration registers

const uint16_t *TS_CAL30  = ((uint16_t *) 0x1FFF75A8);
const uint16_t *TS_CAL110  = ((uint16_t *) 0x1FFF75CA);
const uint16_t *VREF_CAL = ((uint16_t *) 0x1FFF75AA);


#define ADC_GRP1_NUM_CHANNELS 2
/*
 * ADC Conversion group
 * Mode:        Linear buffer, 1 sample of 2 channels, sw triggered, continuous
 * Channels:    0 (vref), 17 (temp sensor)
 */

// Warning, stm32l476 uses "dual mode" and has extra initializers

static const ADCConversionGroup adcgrpcfg1 = {
  .circular     = FALSE,
  .num_channels = ADC_GRP1_NUM_CHANNELS,
  .end_cb       = NULL,
  .error_cb     = NULL,
  .cfgr         = ADC_CFGR_RES_12BITS | ADC_CFGR_CONT, 
  .cfgr2        = 0, 
  .tr1          = ADC_TR(0,4095), // disable watchdog
  .smpr         = {
    ADC_SMPR1_SMP_AN0(ADC_SMPR_SMP_247P5) ,
    ADC_SMPR2_SMP_AN17(ADC_SMPR_SMP_247P5),
  },
  .sqr          = {
    ADC_SQR1_SQ1_N(ADC_CHANNEL_IN0) | ADC_SQR1_SQ2_N(ADC_CHANNEL_IN17),
    0U,
    0U,
    0U
 }
};

static struct {
  adcsample_t vref;
  adcsample_t ts;
} samples;

//static const float CAL_VOLTAGE = 3.0;

void adc_read(float *ts, float *vdd, float *temperature) {
  //*vdd = 3.3;          // nucleo vdd supply
  //*temperature = 21.0; // room temperature
  
  float m = 0.0;
  float b = 0.0;

  m = (float)(110.0-30.0)/((*TS_CAL110) - (*TS_CAL30));
  b = (float) 30.0 - m*(*TS_CAL30);
  // compute m,b from
  // *TS_CAL30,  *TS_CAL110

  // convert the data
  adcConvert(&ADCD1, &adcgrpcfg1, (adcsample_t *) &samples, 1);  
  cacheBufferInvalidate(&samples, sizeof(samples));

  *ts = samples.ts;  // for now we pass back this raw value only

  // Code to compute scaled values goes here !
//(ts / vdd) * 4095
  // compute vdd with samples.vref
  // *VREF_CAL
  *vdd = 3.0 * (*VREF_CAL) / samples.vref;
  //T =  m * (Tmeas * V_R/3.0) + b
  *temperature =  (float)m * ((*ts) * (*vdd)/3.0) + (float) b;
 
}

void adc_init() {
    // start ADC converter
  adcStart(&ADCD1, NULL);
  // enable voltage ref and temperature sensors
  adcSTM32EnableVREF(&ADCD1);
  adcSTM32EnableTS(&ADCD1);
}