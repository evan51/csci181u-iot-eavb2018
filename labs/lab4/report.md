# Lab 4 Report
Evan Von Oehsen 

CSCI 181u

Professor Brown

2/18/2020
## MQTT With Node Red
After a brief introduction to sending and receiving messages in terminal with MQTT, I went on to create a publisher to 'po181u/ev' and a subscriber to 'po181u/#'. I published to 'ev' specifically, because I found all the timestamps being published to /# to be a lot of extra noise. This way, I was able to isolate my messages in a more specific topic. Screenshots below show the Node Red implementation:
![alternative text](img/node-red-1.png)

..and the messages received from my subscrition, through terminal:

![alternative text](img/terminal-1.png)


## Locally Hosted MQTT on Node Red
Following the public MQTT server, I then created a locally hosted one on my machine. It was interesting to see another implementation of MQTT with a smaller scope, not to mention satisfying to not be filled with so many extra messages. I also liked the fact that the messages published were not just timestamps but instead randomly generated messages from the bit of code in the function. Very simple, but it allows us to see what we could execute with our own implementations. The screenshot below shows the Node Red implementation, and logs in the tab to the right:

![alternative text](img/node-red-2.png)

## Flespi
The final step was to use web-based broker flespi. Contrary to the previous implementations in this lab, this MQTT server could be accessed anywhere, as long as the corresponding token is used. 

Setting up a server in flespi was very simple to do. The dashboard was very user friendly. Below is a screenshot of my basic flespi implementation, this time with the topic of 'po181u/test':

![alternative text](img/flespi.png)

As you can see, my first message also published to my terminal window, which used a mosquitto_sub subscription.

## Flespi with TLS

The logical next step for any web-based MQTT server is to make it more secure. Through the internet, anyone can access it with the right token and topic, so adding that extra layer is a sound action to take. With the certificate pre-provided for us, and TSL infrastructure built into Node Red, it was quite easy to hook up my previous Node Red to the flespi server (just needed the URL and token, plug and play), and add a new TLS configuration in which I attached the ca certificate associated with this lab. Below are screenshots of the TLS configuration panel, and the flespi server with my node red faux "temperatures" being published every second, respectively.

![alternative text](img/tls-cert.png)
![alternative text](img/node-flespi.png)

## Reflection

Overall, this lab was straightfoward but also exciting new territory for me. I'd had limited experience working with API calls and tokens from my job, but it had always been myself reading and analysing others' code to learn. I enjoyed generating my own token and servers; doing it yourself is the best way to learn and the most rewarding. I had no real issues, though it took a bit for me to figure out the layout of flespi and how to connect it with node red.
I enjoyed this lab, and am excited about how this material, especially web-based flespi, could be applied to our final projects.
 