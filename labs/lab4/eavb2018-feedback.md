#  Feedback for Evan von Oehsen

I didn't see the Paho publish/subscribe clients.   In terms of the report, please start your reports with an overview that provides a roadmap to the rest of the report.  Also, when your flows include function nodes, please include the corresponding javascript as documentation.

## Grading Rubric

| Component |  Task  | Points Available | Points Awarded|
|-----------|  ----- |----------------:| -------------:|
| All       | Completion           |  60 | |
|           | Communication with MQTT server using command line | 10 | 10 |
|           | Communication with MQTT server using node-red | 10 | 10 |
|           | Building a "thing                                 | 10 | 10 |
|           | Communication with Flespi                         | 10 | 10 |
|           | Securing your flow                                | 10 | 10 |
|           | Building a Paho client                            | 10 | 0 |
| Report    |                      | 40    | |
|           | Overview                                          | 5  | 2 |
|           | Steps taken + issues                              | 15 | 15 |
|           | Discussion of your designs                        | 15 | 14 |
|           | Screenshots of flows/dashboards                   | 5  | 5 |

