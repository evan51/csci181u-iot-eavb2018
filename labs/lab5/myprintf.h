#ifndef __MYPRINTF_H__
#define __MYPRINTF_H__

void printint(int);
void printstring(char *);
void printhex(unsigned int);
void myprintf(const char *,...);

#endif