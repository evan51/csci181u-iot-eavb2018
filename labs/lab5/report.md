# Lab 5

Evan Von Oehsen <br>
po181u <br>
2/25/2020 <br>

### Contents:
Writing/Compiling a "vacuous" program <br/> Writing a "Real" Program <br/> Creating + Structuring Makefiles <br/> Creating a Two Module Program </br> Appendix

## Writing/Compiling a "vacuous" program

As any developer does, my very first program in C was a classic "Hello World". Building a Hello World is quite simple, but executing it can tell you a lot about a particular language. To execute it properly, one needs to know how to write a main method that will run, what modules to import (e.g. standard i/o), how to write a print statement, and of course how to compile the code and run/view the results. We then made it more interesting by adding a loop that prints every 3 seconds. The output in the terminal was simple but satisfying:
![alternative text](img/c-results.png)
This loop was useful to learn, too, since it may come in handy later when writing embedded software that might, say, send a message every three seconds to a server. Now, with a sense of the basics, it was time to move into something a little more interesting.

## Writing a "Real" Program

Next, we wrote a program that actually has a use and a dynamic aspect to it: a replica of the Unix command line command, wc (wordcount). Most of the code was completed for us, however to get the numbers accurate required a few small changes to the end of the code so that it does not count '\n' as a character. It also was a nice opportunity to learn about a few file-specific processes, such as reading all characters from a file via standard input and what the EOF character does. 

Below is a screenshot of both the standard wc command results, as compared to the custom mywc program below it:

![alternative text](img/mywc.png)

## Creating + Structuring Makefiles

Writing single-file programs, such as the printf program above, are simple to compile through the command line. However, more complex programs which require multiple files would be a headache to compile once, not to mention re-compile every time a file with multiple dependencies is edited. Makefile comes in to allow one to specify these dependencies and turn five or six plus commands into one simple "make" command.  

For our mywc program, we were given a makefile to plug and play, which was interesting to interact with. As  shown below, running make the first time compiled and built everything I needed. If ran again, it is smart to know everything is up to date:

![alternative text](img/running-make.png)

However, in our next sub-assignment, we would have to make our own makefiles from scratch.

## Creating a Two Module Program
A 2+ module program is the perfect use case to create a make file. The program I created for this was a simple home-made printf method, which was comprised of a header file, myprintf file, and test file. When compiled, test.o would be a dependency of both test.c and myprintf.h, while myprintf.o would have a dependency of both myprintf.h and myprintf.c. This would mean that any changes to any of the original files would require several to be compiled again. 

The makefile I created was able to solve this, however it took quite a bit of time for me to figure it out. I started by trying to base my work off of the mywc make file that was given to us, however I still found it a little confusing and this setup was more complex. I decided to first map out the dependencies as a graph on a piece of paper, and with Professor Brown's input (and handwriting), I ended up with a diagram like so:

![alternative text](img/diagram.jpg)

Using the above diagram, it was much easier to visualise the dependencies and create a functional makefile. After quite a few tests (using print statements) I was able to create a makefile that worked:

![alternative text](img/makefile.png)

Finally, it was time to get to programming.

The myprintf.c file contains a myprintf function with three helper functions: printint, printhex, and printstring. All three are straightforward from the names, but surprisingly tricky for a first-time C coder. 

To write printstring, I simply pulled some of the code we were given for the main myprintf function, which showed me how to pace through a string character by charater, and allowed me to print these to the log using putchar. This one was the simplest to make.

Next, printint required not just pulling out each digit (using modulus), nor flipping to the proper the order before printing, but also converting these characters to the proper ASCII value before feeding them into putchar. After googling an ASCII table and creating an operation to add the ASCII value of 0 to each integer, I soon learned that all I had to do was add it to '0' instead. That said, it is nice to know how to do it in two different ways. 

Printhex required the same steps, however instead of pacing through each digit, I drew out the remainder of 16 continuously and divided by 16 until there was no remainder left. Then, to ensure the proper hex digit was chosen, I followed the general advice in the assignment and created a lookup table which was a simple string ("0123456789abcdef") which allowed me to seleft the proper value to drop into  putchar. Pretty neat.

My code for all three methods is available in the appendix section.

Making these methods work was new to me, but not overwhelming. What was a bit more difficult was figuring out why I was getting this error:

![alternative text](img/error1.png)

I was terrorized by "segmentation fault: 11" for probably 45 minutes. I had googled around, but all of the suggestions I took led me back to trying to mess with the string pointers in the myprintf function, which was not improving anything upon my changes. As a last-ditch effort, I decided to check my header file. I realized then and there that my printstring and myprintf methods did not have a star (*) after "char", which was incongruous to the parameters in the original funcitons. Adding these in fixed the issue right away, and I was relieved to see this output:

![alternative text](img/correct-output.png)

With each print statement running successfully, I had finished the assignment, making both my first make file and my first multiple-module project in C. 

# Appendix
myprintf helper methods:

``` 
void printint(int i) {
    if (i < 0){
        putchar('-');
        i = i * (-1);
    }
    if (!i) {
        putchar('0');
        return;
    }
        
    int n [10];
    int maxind = -1;
    while (i) {
        maxind++;
        n[maxind] = (i%10 + '0');
        i = i / 10;
    }
    while (maxind+1) {
        putchar(n[maxind]);
        maxind--;
    }
}

void printhex(unsigned int i) {
    if (i < 0){
        putchar('-');
        i = i * (-1);
    }
    if (!i) {
        putchar('0');
        return;
    }
    int n [10];
    int maxind = -1;
    while (i) {
        maxind++;
        char digit;
        const char *Hexdigits = "0123456789abcdef";
        digit = Hexdigits[i%16];
        n[maxind] = digit;
        i = i / 16;
    }
    while (maxind+1) {
        putchar(n[maxind]);
        maxind--;
    }
}

void printstring(char *s) {
    const char *p;
    for (p = s; *p != '\0'; p++) {
        putchar(*p);
    }
}
```