#include <stdarg.h>
#include <stdio.h>

void printint(int i) {
    if (i < 0){
        putchar('-');
        i = i * (-1);
    }
    if (!i) {
        putchar('0');
        return;
    }
        
    int n [10];
    int maxind = -1;
    while (i) {
        maxind++;
        n[maxind] = (i%10 + '0');
        i = i / 10;
    }
    while (maxind+1) {
        putchar(n[maxind]);
        maxind--;
    }
}

void printhex(unsigned int i) {
    if (i < 0){
        putchar('-');
        i = i * (-1);
    }
    if (!i) {
        putchar('0');
        return;
    }

    int n [10];
    int maxind = -1;
    while (i) {
        maxind++;
        char digit;
        const char *Hexdigits = "0123456789abcdef";
        digit = Hexdigits[i%16];
        n[maxind] = digit;
        i = i / 16;
    }
    while (maxind+1) {
        putchar(n[maxind]);
        maxind--;
    }
}

void printstring(char *s) {
    const char *p;
    for (p = s; *p != '\0'; p++) {
        putchar(*p);
    }
}

void myprintf(const char *fmt, ...) {
    const char *p;
    va_list argp;
    int i;
    char *s;

    va_start(argp, fmt);

    for (p = fmt; *p != '\0'; p++) {
        if (*p != '%') {
            putchar(*p);
            continue;
        }
        switch (*++p) {
        case 'c':
            i = va_arg(argp, int);
            putchar(i);
            break;

        case 'd':
            i = va_arg(argp, int);
            printint(i);
            break;

        case 's':
            s = va_arg(argp, char *);
            printstring(s);
            break;

        case 'x':
            i = va_arg(argp, int);
            printhex(i);
            break;

        case '%':
            putchar('%');
            break;
        }
    }
    va_end(argp);
}