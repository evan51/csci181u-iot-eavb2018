#  Feedback for Evan von Oehsen

You should include a text overview that lays out the document structure.   

Rather than using screenshots to capture code, markdown has syntax to do code highlighting as in

```c
void main(void) {
    printf("hello world\n");
}
```
Also, rather than including all of the code as an appendix, it is clearer if you include
fragments in line with the text that describes the code.

I see you chose to build up your integer string in an array.  It looks like your code might produce an extra leading zero.  

A more idiomatic way to handle `printstring` is

```c
void printstring(char *s) {
    while (s && *s) {
        putchar(*s++);
    }
}
```

## Grading Rubric

| Component |  Task  | Points Available | Points Awarded|
|-----------|  ----- |----------------:| -------------:|
| All       | Completion           |  60 | |
|           | Vacuous Program | 5 | 5 |
|           | Word count | 20 | 20 |
|           | Makefile                               | 5 | 5 |
|           | Two module program                        | 30 | 30 |
| Report    |                      | 40    | |
|           | Overview                                          | 5  | 2 |
|           | Steps taken + issues                              | 15 | 15 |
|           | Discussion of your code and example output                        | 20 | 18 |


