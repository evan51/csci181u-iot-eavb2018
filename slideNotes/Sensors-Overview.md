# Sensors-Overview Summary
### by Evan Von Oehsen


In many cases, the focal point of an iOT system will be the sensors it leverages. There are a variety of different types of sensors, several of which are on our lab board, yet they all rely on one from just a handful of different hardware interfaces to get them working. 

There are a variety of sensors we can use to observe the world around us. Environmental sensors encapsulate anything you’d see on the weather channel–temperature, humidity, air pressure, in addition to things like proximity and touch. Position sensors focus more on relative and absolute locations with GPS, magnetometer, and distance which, paired with inertial sensors, can provide the basic necessities for a vehicle moving in 3 dimensions, such as an airplane or a drone. Of course, cameras and microphones are sensors. Mechanical, electrical, and medical sensors cover the remaining categories, each of which have their sets of use cases.

Our lab board, the ST X-NUCLEO-IKS01A3, is a “motion, MEMS, and environmental sensor board system” that has a lot of versatility with its broad spectrum of sensors and compatibility with both our NUCLEO board and any others with an Arduino UNO R3 connector layout. While we’ve leveraged the temperature and voltage sensors thus far, there are plenty more including humidity and pressure sensors, an accelerometer, and a gyroscope.

Our lab devices use several from a selection of hardware interfaces. All of them use an interface called I2C, which provides the interface for the bus. Most of them also implement interrupts and SPI, two important interfaces which are used to let the processor know when a specific event needs attention, and pass data between the microcontroller, respectively. 

Looking at the block diagrams of various sensors, a lot of similarities can be pulled from them; the majority of them seem to implement an analog to digital converter, a control logic module, the actual sensors, clock and timing and a handful of the various interfaces we learned earlier such as SPI, I2C, and interrupts.

To learn more about a specific sensor, we can review its data sheet. The data sheet defines what it’s for, what its applications are, and more specific specs such as the supply voltage, range, accuracy, sensitivity, and other basic functionality and applications information. More specific information on parameters such as the range, accuracy, and sensitivity can be find in the spec table later on in the data sheet. Following that, register address information can be found, which is important for a programmer to know should they want to where to access the information. 

Sensors often require calibration as well; with temperature and humidity, for example, calibration constants are used to find the actual humidity by using a linear function to convert voltage to the proper value. 

To actually talk to sensors, low-level and high-level drivers are used. Lower level drivers provide access to registers, procedure interfaces, and other ways to access the bits defined within the registers without higher-level functionality. Higher-level drivers provide routines that fit directly into the operating system to interact with the relevant information. ChibiOS, for example, is a higher level driver that provides macro functions, a portable interface, data calibration and computing, and still provides all the lower level details desired.