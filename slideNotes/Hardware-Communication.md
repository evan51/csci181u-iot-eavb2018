# Hardware Communication - Summary
### by Evan Von Oehsen

The three most common communication protocols used by sensors are Serial, SPI, and I2C. Serial, being one 

Serial protocol, the oldest protocol, is an asynchronous protocol that depends on local clocks. Its frame is marked by a start and a stop bit. It is used for UART, GPS receivers, cell phone modules, among others. Typically, the serial port is used to communicate with an external device (e.g. our Nucleo boards). For UART, there are receive and transmit signals, in addition to others that provide flow control, though they don't always perform as expected.

Serial protocol is all about timing, and to aid with this, the sender and receiver will each have local clocks synced within ~2%. They are then programmed to have the same baud rate, and will have a sample rate of about 16x. This way, the receiver can detect when the start bit occurs, and can estimate when the subsequent bits will arrive. If the clocks are synced somewhere outside 2%, they may slowly drift apart. 

Serial driver software is in charge of configuring the baud rate and protocol, in addition to manage sending and receiving buffers and reading/transmitting bytes. There are two types of these driver: polling, and interrupt driven. Polling is very simple, but very inefficient as it has to make constant checks. Interrupt driven is much more efficient as it can simply wait for the hardware to make an interrupt request.

SPI and I2C are both point-to-point synchronouus protocols. SPI is a uni-directional protocal with a single clock with a master and one or more slaves. There are two data signals, MOSI (Master out slave in) and MISO (Master in slave out). The timing of the whole thing is driven by the master. It is very simple and can operate at high speeds.

During transmission, one bit is transferred out from master the master register to slave register via MOSI, and one bit is transferred in via MISO. This is simultaneous, bi-directional commuunication.

A lot of devices use SPI protocol, one example is the serial EEPROM memory protocol. This protocol implements byte, or word, transfer. EEPROM has control signals and SO and SI for slave out and slave in. Another common SPI device is a color LCD display with a panel and a controler. SPI would allow localized blocks of pixel updates. A third example would be SD card transactions, in which an SD card would be inserted into a machine having a SPI Driver, with an SD Driver, Fat Driver, and Application stacked on top.

Lastly, I2C ocntains a 2 wire broadcast bus where single master and multiple slaves can sit on the same bus. The two wires are a sincle clock signal (SCL) and shared data signal (SDA). I2C can be thought of as transactions, write and read. Every transaction is initiated by the master, which sends out the start bit with the slave's address and whether it's a read or write signal. There is then an acknowledgement from the slave. Data transfer is ended either by a negative slave acknowledgement, or master has no more data to send. P signal ends the stream.

An I2C example is the Wii nunchuck, which has a set of addresses for individual information pieces from joystick, accelerometer, and buttons. The console using it is constantly pulling from the controller.

ADC converters take analog signal and produce digital representations, while DAC do the opposite. DAC are important for proceses like voltage generation for speaker drivers. More importantly, it is necessary to move data at a steady rate, it is important for them to be read/written to with time-triggered DMA. These converters are pretty complicated, specifically STM32 ADC, as there are many input sources, many of which external, and there is hardware that selects which signals are being sampled. When sampling audio, it would be important that the two stereo channels be sampled at a fixed frequency. 

All in all, these protocols come together to make the sensors we use in our IoT systems work, without which they would be useless.