# csci181u iot

This is the template for student repositories

## Things to do after forking your repo

In order for the the lab-instructions module to be added youneed to execute the following at a terminal in the top level of your repository

```
$ git submodule init
$ git submodule update
```

Remember that the '$' is a placeholder for your terminal prompt !!  You should then be
able to see the files in the lab-instructions directory

```
$ ls lab-instructions
README.md
   ... other files ...
```
