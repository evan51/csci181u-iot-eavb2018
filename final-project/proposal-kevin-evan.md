# Feedback for Kevin Ayala and Evan Von Oehsen


## Problem Statement

I'm a little unclear on the specific project goals. As I understand the proposal you every inventory item will have an RFID tag and you would detect when the item leaves the room (you might want to study how libraries track books electronically).  In addition, you will detect when people enter/leave the room.

You also need to be a little clearer on what you build vs specify and what a successful outcome would be.


## Sensors

### Type

RFID, motion (presence)   I think you need to examine the options for tracking presence/absence of people.  Also, many RFID systems require no energy for the tag.

### Number and Data Rate

While there might be hundreds of items, do you need to track their state (present/not present) over time, or just state changes ?  In the latter case, the amount of "real-time" data is likely to be small.


## Data Storage

This part is a bit unclear.  How much data is there ?  


## Visualization


## Other Issues

* Security
* Privacy