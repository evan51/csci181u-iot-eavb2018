# Final Project Proposal #
Kevin Ayala
Evan Von Oehsen


## Our Idea ##

We plan to create an inventory system for one of the technology labs at school. It shuold implement RFID scanners to track equipment, combined with a motion sensor to track when someone enters or leaves the room. 

## Sensors + Hardware ##
<ul> 
    <li> X-NUCLEO-NFC05A1: NFC/RFID Reader and Writer
    <li> Nucleo IKS01A2: Motion Sensor
    <li> Nucleo STM32
    <li> iMac Computer
    <li> WRL-14151 RFID transmitter stickers
</ul>

## Networking ##

We can use MQTT to send JSON packages to the server where we'd host the database - we could use something like Cloudflare to host both the code and database.

We could use a visualization tool, such as Grafana, to track attendance and movement of equipment in/out of the lab.

## Infrastructure ## 

Geography - we would place the RFID and motion sensor at the door. This would mean that it can track the door's movement to know when someone leaves or enters, and can scan the items in that moment. We would only need those two sensors. 

Data Rates - we'd have to make sure the person stays long enough for the item to be scanned. 

Data Storage - there are hundreds of items in the lab, so we'd probably want a backup drive connected to the device as well as sending it to the server's database. 

Data Visualization - we would ideally have a screen to let people know what they've checked out, to make sure they actually stop and scan their items, and potentially even to show what's available. 

Energy - we can power the devices by connecting it to a computer. We'd have to worry about the RFID sticker batteries, but they look to be self-sustaining for a period of 3-5 years, typically. Since we'd need a screen and extra storage for the board anyway, we could have it hooked up to a machine that would display that information, like one of the lab laptops. 

